# Game API 

## Instalation
### Clone
1. `git clone -b develop git@gitlab.com:luky/game-stats.git` or clone a branch
1. Instal `docker` and `docker-compose`
1. Create **intranet**
    * `docker network create --attachable --subnet 172.21.0.0/16 --gateway 172.21.0.1 intranet`
    
---
- *To clone specific branch, use  `git clone -b <branch> <repo> <target_folder>`*

### Register in hosts
1. Put `172.21.130.20 gamee.docker` line in your `/etc/hosts` 

### Start composition
1. `cd <projectDir> && chmod +x docker.sh`
1. *OPTIONAL:* Enable debug mode by putting `DEBUG_MODE=1` into `env.local` file located in project root directory
1. `./docker.sh run`
1. `./docker.sh php bin/console.php mig:reset`
1.  [https://gamee.docker/](https://gamee.docker/) should work now

## Docker.sh  commands

* `./docker.sh run` 
* `./docker.sh stop`
* `./docker.sh exec <container> <command>` - run <command> inside <container>
* `./docker.sh bash <container>` - entry inside <container> terminal (ssh like)
* `./docker.sh composer <command>` - run composer <command> inside php container
* `./docker.sh php <command>` - run <command> (or file) inside php container
* `./docker.sh db <command>` - run `mysql` inside database container 
### Code Quality tools
* `./docker.sh lint` - run phplint checks inside php container
* `./docker.sh stan` - run phpstan checks inside php container
* `./docker.sh test` - run codecept tests inside php container
