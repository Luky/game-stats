#!/usr/bin/env bash

# -----------------------------------------
# Init Constants
# -----------------------------------------
ACTION=$1; shift # Action is already saved, so we can shift first arg

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# -----------------------------------------
# Config Variables
# -----------------------------------------


PROJECT_NAME=gamee

NGINX_IP=172.21.130.20

# -----------------------------------------
# Docker tools checkout
# -----------------------------------------

source ./_injector.sh

# -----------------------------------------
# Init
# -----------------------------------------

touch $DIR/.env.local

# -----------------------------------------
# Arg's
# -----------------------------------------

OPT_FOLLOW=1
OPT_BUILD=1
OPT_PRODUCTION=0

for var in "$@"; do
	case "$var" in
		--no-follow)
			OPT_FOLLOW=0;;
		--no-build)
			OPT_BUILD=0;;
		--production)
			OPT_PRODUCTION=1;;
		*)
			;;
	esac
done


# -----------------------------------------
# Helper Methods
# -----------------------------------------

buildAction() {
	docker build $DIR/_docker/php --build-arg DOCKER_UID=$UID --tag $PROJECT_NAME-php --pull
	docker build $DIR/_docker/nginx --build-arg DOCKER_UID=$UID --tag $PROJECT_NAME-nginx

	if [[ $OPT_PRODUCTION = "0" ]]; then
		docker build $DIR/_docker/db --build-arg DOCKER_UID=$UID --tag $PROJECT_NAME-db
	fi
}


_dockerCompose() {
	if [[ $OPT_PRODUCTION = "1" ]]; then
		docker-compose -f docker-compose.prod.yml $*
	else
		docker-compose $*
	fi
}

# -----------------------------------------
# Actions
# -----------------------------------------

if [[ $ACTION == "run" ]]; then
	_dockerCompose stop

	injector nginx root
	injector php root

	buildAction

	_dockerCompose up -d

	docker network disconnect intranet $PROJECT_NAME-nginx || true
	docker network connect intranet $PROJECT_NAME-nginx --ip $NGINX_IP


	DOCKER_LOGS=''
	if [[ $OPT_FOLLOW == "1" ]]; then
		DOCKER_LOGS='-f'
	fi
	_dockerCompose logs $DOCKER_LOGS --tail="30"


elif [[ $ACTION == "stop" ]]; then
	_dockerCompose stop


elif [[ $ACTION == 'console' ]]; then
	docker exec -it $PROJECT_NAME-php php bin/console.php $*

elif [[ $ACTION == 'check' ]]; then
	$DIR/docker.sh lint
	$DIR/docker.sh stan

elif [[ $ACTION == 'stan' ]]; then
	docker exec -t $PROJECT_NAME-php composer dump -q
	docker exec -t $PROJECT_NAME-php php vendor/bin/phpstan analyse src "$@" -c .phpstan.neon --ansi

elif [[ $ACTION == 'lint' ]]; then
	docker exec -t $PROJECT_NAME-php php vendor/bin/parallel-lint --exclude vendor src $*

elif [[ $ACTION == 'test' ]]; then
	docker exec -t $PROJECT_NAME-php php vendor/bin/codecept $*

elif [[ $ACTION == 'php' ]]; then
	docker exec -it $PROJECT_NAME-php php $*

elif [[ $ACTION == 'composer' ]]; then
	docker exec -t $PROJECT_NAME-php composer $*

elif [[ $ACTION == 'log' ]]; then
	_dockerCompose logs

else
	echo "Unknown action '$ACTION'"
fi

exit 0
