#!/usr/bin/env bash


echo "Install Composer"
composer install --no-interaction

ROOT=/var/docker

TEMP_DIR=$ROOT/temp
TEMP_CACHE_DIR=$ROOT/temp/cache

LOG_DIR=$ROOT/log

if [ -e $TEMP_DIR ]
then
	echo "Temp already exists\n"
else
	mkdir $TEMP_DIR -m 777
fi

if [ -e $TEMP_CACHE_DIR ]
then
	rm -rf $TEMP_CACHE_DIR/*
	echo "Temp/cache already exists\n"
else
	mkdir $TEMP_CACHE_DIR -m 777
fi

if [ -e $LOG_DIR ]
then
	echo "Log already exists\n"
else
	mkdir $LOG_DIR -m 777
fi

echo "Change ownership of log, migrations and temp to host"
chown dockeruser:dockeruser -R log migrations temp

echo "Make writable Log and Temp dirs"
chmod 777 -R $TEMP_DIR $TEMP_CACHE_DIR $LOG_DIR


ADMINER_PATH=$ROOT/adminer.php
ADMINER_WWW_PATH=$ROOT/www/adminer.php


if [ $DEBUG_MODE = "1" ]
then
	if [ -e $ADMINER_WWW_PATH ]; then
		echo "Adminer already exists"
	else
		ln -s $ADMINER_PATH $ADMINER_WWW_PATH
	fi

else
	if [ -e $ADMINER_WWW_PATH ]; then
		rm $ADMINER_WWW_PATH
	fi

fi


# Create debug Mode
echo "<?php" > $ROOT/src/debugMode.php

if [ $DEBUG_MODE = "1" ]; then
	echo "DEBUG mode ENABLED"

	echo "return true;" >> $ROOT/src/debugMode.php
else
	echo "DEBUG mode DISABLED"

	echo "return false;" >> $ROOT/src/debugMode.php
fi



echo "=========================================================================="
php -v
echo "=========================================================================="
env
echo "=========================================================================="

php bin/console.php migrations:reset

echo "--------------------------------------------------------------------------"

php-fpm
