<?php declare(strict_types = 1);

use Codeception\Module\Asserts;

class PositionCounterCest
{
	public function oneItem(Asserts $I): void
	{
		$counter = new \Gamee\PositionCounter([10]);
		
		$I->assertSame(1,$counter->getPosition(10));
	}
	
	public function twoItems(Asserts $I): void
	{
		$counter = new \Gamee\PositionCounter([333, 10]);
		
		$I->assertSame(2, $counter->getPosition(10));
		$I->assertSame(1, $counter->getPosition(333));
	}
	
	public function unorderedSet(Asserts $I): void
	{
		$counter = new \Gamee\PositionCounter([90, 5, 10, 30]);
		
		$I->assertSame(1, $counter->getPosition(90));
		$I->assertSame(2, $counter->getPosition(30));
		$I->assertSame(3, $counter->getPosition(10));
		$I->assertSame(4, $counter->getPosition(5));
	}
	
	
	public function samePosition(Asserts $I): void
	{
		$counter = new \Gamee\PositionCounter([90, 5, 30, 30]);
		
		$I->assertSame(1, $counter->getPosition(90));
		$I->assertSame(2, $counter->getPosition(30));
		$I->assertSame(2, $counter->getPosition(30));
		$I->assertSame(4, $counter->getPosition(5));
	}
	
	public function allTenSame(Asserts $I): void
	{
		$counter = new \Gamee\PositionCounter([90,90,90,90,90,90,90,90,90,90]);
		
		$I->assertSame(1, $counter->getPosition(90));
		$I->assertSame(1, $counter->getPosition(90));
		$I->assertSame(1, $counter->getPosition(90));
		$I->assertSame(1, $counter->getPosition(90));
		$I->assertSame(1, $counter->getPosition(90));
		$I->assertSame(1, $counter->getPosition(90));
		$I->assertSame(1, $counter->getPosition(90));
		$I->assertSame(1, $counter->getPosition(90));
		$I->assertSame(1, $counter->getPosition(90));
		$I->assertSame(1, $counter->getPosition(90));
	}
	
	
	public function invalidItem(Asserts $I): void
	{
		$I->expectThrowable(\Gamee\ValueDoesntExistsInPositionCounterException::class, function () {
			$counter = new \Gamee\PositionCounter(
				[
					5,
					333,
				]
			);
			
			$counter->getPosition(55);
		});
		
	}
}
