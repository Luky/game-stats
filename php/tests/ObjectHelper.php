<?php declare(strict_types = 1);

class ObjectHelper
{
	public static function createSendScoreBlock($id, $userId, $gameId, $score): array
	{
		return [
			'jsonrpc' => '2.0',
			'method'  => 'send_score',
			'id'      => $id,
			'params'  => [
				'user_id' => $userId,
				'game_id' => $gameId,
				'score'   => $score,
			],
		];
	}
	
	public static function createSendScoreResponse($id): array
	{
		return [
			'id'      => (string) $id,
			'success' => 0,
		];
	}
}
