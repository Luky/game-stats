<?php

require_once __DIR__ . '/ObjectHelper.php';

function _resetMigration()
{
	
	/** @var \Nette\DI\Container $container */
	$container = require __DIR__ . '/../src/bootstrap.php';
	
	$migrationsReset = $container->getByType(\Nextras\Migrations\Bridges\SymfonyConsole\ResetCommand::class);
	$migrationsReset->run(
		new \Symfony\Component\Console\Input\ArrayInput([]),
		new \Symfony\Component\Console\Output\NullOutput()
	);
}

