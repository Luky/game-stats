<?php declare(strict_types = 1);

class BatchRequestCest
{
	public function _before(): void
	{
		_resetMigration();
	}
	
	public function batchRequest(\ApiTester $I): void
	{
		$I->sendPOST(
			'/',
			[
				ObjectHelper::createSendScoreBlock(1, 4, 1, 100),
				ObjectHelper::createSendScoreBlock(2, 3, 1, 200),
				ObjectHelper::createSendScoreBlock(3, 2, 1, 300),
				ObjectHelper::createSendScoreBlock(4, 1, 1, 400),
				[
					'jsonrpc' => '2.0',
					'method'  => 'get_highscore',
					'id'      => 4,
					'params'  => [
						'game_id' => 1,
					],
				],
			]
		);
		
		$I->seeResponseContainsJson(
			[
				ObjectHelper::createSendScoreResponse(1),
				ObjectHelper::createSendScoreResponse(2),
				ObjectHelper::createSendScoreResponse(3),
				ObjectHelper::createSendScoreResponse(4),
				[
					'success' => [
						['position' => 1,'score' => 400, 'user' => ['id' => 1]],
						['position' => 2,'score' => 300, 'user' => ['id' => 2]],
						['position' => 3,'score' => 200, 'user' => ['id' => 3]],
						['position' => 4,'score' => 100, 'user' => ['id' => 4]],
					],
				],
			]
		);
		
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
	
	
	public function batchRequestSameScore(\ApiTester $I): void
	{
		
		$I->sendPOST(
			'/',
			[
				ObjectHelper::createSendScoreBlock(1, 4, 1, 100),
				ObjectHelper::createSendScoreBlock(2, 3, 1, 200),
				ObjectHelper::createSendScoreBlock(3, 2, 1, 200),
				ObjectHelper::createSendScoreBlock(4, 1, 1, 400),
				ObjectHelper::createSendScoreBlock(5, 6, 1, 200),
				ObjectHelper::createSendScoreBlock(6, 7, 1, 200),
				ObjectHelper::createSendScoreBlock(7, 8, 1, 200),
				ObjectHelper::createSendScoreBlock(8, 9, 1, 200),
				[
					'jsonrpc' => '2.0',
					'method'  => 'get_highscore',
					'id'      => 9,
					'params'  => [
						'game_id' => 1,
					],
				],
			]
		);
		
		$I->seeResponseContainsJson(
			[
				ObjectHelper::createSendScoreResponse(1),
				ObjectHelper::createSendScoreResponse(2),
				ObjectHelper::createSendScoreResponse(3),
				ObjectHelper::createSendScoreResponse(4),
				ObjectHelper::createSendScoreResponse(5),
				ObjectHelper::createSendScoreResponse(6),
				ObjectHelper::createSendScoreResponse(7),
				ObjectHelper::createSendScoreResponse(8),
				[
					'id' => 9,
					'success' => [
						['position' => 1,'score' => 400, 'user' => ['id' => 1]],
						['position' => 2,'score' => 200, 'user' => []],
						['position' => 2,'score' => 200, 'user' => []],
						['position' => 2,'score' => 200, 'user' => []],
						['position' => 2,'score' => 200, 'user' => []],
						['position' => 2,'score' => 200, 'user' => []],
						['position' => 2,'score' => 200, 'user' => []],
						['position' => 8,'score' => 100, 'user' => ['id' => 4]],
					],
				],
			]
		);
		
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
	
}
