<?php declare(strict_types = 1);

class InvalidRequestCest
{
	public function invalidRequestObject(\ApiTester $I): void
	{
		$I->sendPOST('/', ['some' => 'obj']);
		
		$I->seeResponseContains('{"id":null,"jsonrpc":"2.0","error":{"code":-32700,"message":"Parse Error"}}');
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
	
	public function emptyRequestObject(\ApiTester $I): void
	{
		$I->sendPOST('/', []);
		
		$I->seeResponseContains('{"id":null,"jsonrpc":"2.0","error":{"code":-32700,"message":"Parse Error"}}');
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
	
	public function invalidVersionRequest(\ApiTester $I): void
	{
		$I->sendPOST('/', ['jsonrpc' => '2.3']);
		
		$I->seeResponseContains('{"id":null,"jsonrpc":"2.0","error":{"code":30,"message":"Invalid Json RPC version"}}');
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
	
	public function invalidBatchVersionRequest(\ApiTester $I): void
	{
		$I->sendPOST('/', [['x' => 'y']]);
		
		$I->seeResponseContains(
			'[{"id":null,"jsonrpc":"2.0","error":{"code":30,"message":"Invalid Json RPC version"}}]'
		);
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
	
	public function noMethod(\ApiTester $I): void
	{
		$I->sendPOST('/', ['jsonrpc' => '2.0']);
		
		$I->seeResponseContains('{"id":null,"jsonrpc":"2.0","error":{"code":-32601,"message":"Method not found"}}');
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
	
	public function methodNotFound(\ApiTester $I): void
	{
		$I->sendPOST('/', ['jsonrpc' => '2.0', 'method' => 'asda']);
		
		$I->seeResponseContainsJson(['id' => null,'jsonrpc' => '2.0', 'error' => ['code' => -32601, 'message' => 'Method not found']]);
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
	
	
	
}
