<?php declare(strict_types = 1);

class SendScoreCest
{
	public function _before(): void
	{
		
		_resetMigration();
	}
	
	public function valid(\ApiTester $I): void
	{
		$reqId = random_int(10, 150);
		
		$I->sendPOST(
			'/',
			[
				'jsonrpc' => '2.0',
				'method'  => 'send_score',
				'id'      => $reqId,
				'params'  => [
					'user_id' => 5,
					'game_id' => 3,
					'score'   => 1000,
				],
			]
		);

//		$I->seeResponseContains('xx');
		$I->seeResponseContainsJson(
			[
				'id'      => "$reqId",
				'jsonrpc' => '2.0',
				'success' => 0,
			]
		);
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
	
	public function userNotFound(\ApiTester $I): void
	{
		$I->sendPOST(
			'/',
			[
				'jsonrpc' => '2.0',
				'method'  => 'send_score',
				'id'      => 13,
				'params'  => [
					'score'   => 5000,
					'user_id' => 55,
					'game_id' => 5,
				],
			]
		);
		
		$I->seeResponseContainsJson(
			[
				'error' => [
					'code'    => -32801,
					'message' => "User with given id '55' doesn't exists.",
				],
			]
		);
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
	
	public function gameNotFound(\ApiTester $I): void
	{
		$I->sendPOST(
			'/',
			[
				'jsonrpc' => '2.0',
				'method'  => 'send_score',
				'id'      => 13,
				'params'  => [
					'score'   => 5000,
					'user_id' => 5,
					'game_id' => 55,
				],
			]
		);
		
		$I->seeResponseContainsJson(
			[
				'error' => [
					'code'    => -32802,
					'message' => "Game with given id '55' doesn't exists.",
				],
			]
		);
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
}
