<?php declare(strict_types = 1);

class GetHighscoreCest
{
	public function _before(): void
	{
		_resetMigration();
	}
	
	public function getEmptyHighscore(\ApiTester $I): void
	{
		$I->sendPOST(
			'/',
			[
				'jsonrpc' => '2.0',
				'method'  => 'get_highscore',
				'id'      => 13,
				'params'  => [
					'game_id' => 13,
				],
			]
		);
		
		$I->seeResponseContainsJson(['success' => []]);
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$I->seeResponseIsJson();
	}
	

}
