<?php declare(strict_types = 1);

/** @var \Nette\DI\Container $container */
$container = require __DIR__ . '/../src/bootstrap.php';

/** @var \Symfony\Component\Console\Application $console */
$console = $container->getByType(Symfony\Component\Console\Application::class);

$services = $container->findByType(Symfony\Component\Console\Command\Command::class);

foreach ($services as $service) {
	$console->add(
		$container->getService($service)
	);
}

exit($console->run());
