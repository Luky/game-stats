<?php declare(strict_types = 1);

return [
	'parameters' => [
		'project' => [
			'version' => date('Ymd-Hi'),
		],
	],
];
