<?php declare(strict_types = 1);

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Tracy\Debugger;

$container = require __DIR__ . '/../src/bootstrap.php';

/** @var App $app */
$app = $container->getByType(App::class);

$app->setErrorHandler(
	function (Request $req, Response $res, \Throwable $e) {
		Debugger::exceptionHandler($e);
		
		return $res;
	}
);
$app->setPhpErrorHandler(
	function (Request $req, Response $res, \Throwable $e) {
		Debugger::exceptionHandler($e);
		
		return $res;
	}
);

$app->setNotFoundHandler(
	function (Request $req, Response $res) {
		return $res
			->withStatus(400)
			->withJson(
				[
					'status'  => 'error',
					'code'    => 400,
					'message' => "Path `{$req->getUri()->getPath()}` not found",
				]
			);
	}
);

$app->setNotAllowedHandler(
	function (Request $req, Response $res, $methods) {
		$statusCode = 405;
		
		return $res->withStatus($statusCode)
			->withHeader('Allow', implode(', ', $methods))
			->withJson(
				[
					'status'  => 'error',
					'code'    => $statusCode,
					'message' => 'Method must be one of: ' . implode(', ', $methods),
				]
			);
	}

);

$app->run();
