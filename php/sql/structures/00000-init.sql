-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `game`;
CREATE TABLE `game` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY
);


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(64) NOT NULL
) COLLATE 'utf8mb4_czech_ci';


DROP TABLE IF EXISTS `score`;
CREATE TABLE `score` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `game_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `score` int NOT NULL,
  `created_at` date NOT NULL,
  FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);


