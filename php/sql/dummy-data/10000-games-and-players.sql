-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `game` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10);

SET NAMES utf8mb4;

INSERT INTO `user` (`id`, `name`) VALUES
(1,	'Becky Delacruz'),
(2,	'Taran Bevan'),
(3,	'Viktoria Rivera'),
(4,	'Cieran Edmonds'),
(5,	'Aras Cole'),
(6,	'Jayden-James Watts'),
(7,	'Cohan Prince'),
(8,	'Sameer Castro'),
(9,	'Tony Levine'),
(10,	'Stanley Hodson');

-- 2019-01-06 19:32:50
