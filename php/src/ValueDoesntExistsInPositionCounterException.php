<?php declare(strict_types = 1);

namespace Gamee;

class ValueDoesntExistsInPositionCounterException extends \LogicException
{
}
