<?php declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';

/** @var bool|array $debugMode */
$debugMode = false;
if (file_exists(__DIR__ . '/debugMode.php')) {
	$debugMode = require __DIR__ . '/debugMode.php';
}

$configurator = new Nette\Configurator;
$configurator->setTimeZone('Europe/Prague');

$configurator->setDebugMode($debugMode);

$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->addConfig(__DIR__ . '/../config/config.neon');

$configurator->addDynamicParameters(
	[
		'_server' => $_SERVER,
	]
);

return $configurator->createContainer();
