<?php declare(strict_types = 1);

namespace Gamee;

class ConfigService
{
	/** @var string */
	private $cdnUrl;
	/** @var string */
	private $cdnPath;
	
	/**
	 * @param mixed[] $config
	 */
	public function __construct(array $config)
	{
//		$this->cdnPath = $config['cdn']['path'];
		$this->cdnUrl = $config['cdn']['url'];
		
		
	}
	
	public function getCdnUrl(): string
	{
		return $this->cdnUrl;
	}
	
	public function getCdnPath(): string
	{
		return $this->cdnPath;
	}
	
	public function getItemPath(string $token): string
	{
		return "{$this->getCdnPath()}/temp/{$token}.jpg";
	}
}
