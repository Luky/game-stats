<?php declare(strict_types = 1);

namespace Gamee\Model;

class ModelException extends \LogicException
{
}
