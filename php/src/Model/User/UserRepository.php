<?php declare(strict_types = 1);

namespace Gamee\Model\User;

use Nextras\Orm\Collection\ICollection;

class UserRepository extends \Nextras\Orm\Repository\Repository
{
	/** @return string[] */
	public static function getEntityClassNames(): array
	{
		return [User::class];
	}
	
	/**
	 * @return \Gamee\Model\User\User[]|ICollection
	 */
	public function findBy(array $where = []): ICollection
	{
		return parent::findBy($where);
	}

}
