<?php declare(strict_types = 1);

namespace Gamee\Model\User;

use Nextras\Orm\Entity\Entity;

/**
 * @property int    $id               {primary}
 * @property string $name
 */
class User extends Entity
{
	public function __construct(int $id, string $name)
	{
		parent::__construct();
		
		$this->id = $id;
		$this->name = $name;
	}
	
	public function toApi(): array
	{
		return [
			'id' => $this->id,
			'name' => $this->name,
		];
	}
	
}
