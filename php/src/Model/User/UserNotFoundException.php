<?php declare(strict_types = 1);

namespace Gamee\Model\User;

use Gamee\Model\EntityNotFoundException;

final class UserNotFoundException extends EntityNotFoundException
{
}
