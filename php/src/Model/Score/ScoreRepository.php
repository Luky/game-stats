<?php declare(strict_types = 1);

namespace Gamee\Model\Score;

use Nextras\Orm\Collection\ICollection;

class ScoreRepository extends \Nextras\Orm\Repository\Repository
{
	/** @return string[] */
	public static function getEntityClassNames(): array
	{
		return [Score::class];
	}
	/**
	 * @return \Gamee\Model\Score\Score[]|ICollection
	 */
	public function findBy(array $where = []): ICollection
	{
		return parent::findBy($where);
	}

}
