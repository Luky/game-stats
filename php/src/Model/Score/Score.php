<?php declare(strict_types = 1);

namespace Gamee\Model\Score;

use Nextras\Orm\Entity\Entity;

/**
 * @property-read  int               $id             {primary}
 * @property int                     $score
 *
 * @property \Gamee\Model\Game\Game  $game           {m:1 \Gamee\Model\Game\Game, oneSided=true}
 * @property \Gamee\Model\User\User  $user           {m:1 \Gamee\Model\User\User, oneSided=true}
 *
 *
 * @property-read \DateTimeImmutable $createdAt
 */
class Score extends Entity
{
	public function __construct(int $score, \Gamee\Model\User\User $user, \Gamee\Model\Game\Game $game)
	{
		parent::__construct();
		
		$this->user = $user;
		$this->game = $game;
		$this->score = $score;
		
		$this->setReadOnlyValue('createdAt', new \DateTimeImmutable());
	}
	
	public function toApi(): array
	{
		return [
			'score'     => $this->score,
			'user'      => $this->user->toApi(),
			'game'      => $this->game->toApi(),
			'createdAt' => $this->createdAt->format('c'),
		];
	}
}
