<?php declare(strict_types = 1);

namespace Gamee\Model\Game;

use Nextras\Orm\Collection\ICollection;

class GameRepository extends \Nextras\Orm\Repository\Repository
{
	/** @return string[] */
	public static function getEntityClassNames(): array
	{
		return [Game::class];
	}
	
	/**
	 * @return \Gamee\Model\Game\Game[]|ICollection
	 */
	public function findBy(array $where = []): ICollection
	{
		return parent::findBy($where);
	}
	
}
