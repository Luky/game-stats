<?php declare(strict_types = 1);

namespace Gamee\Model\Game;

use Nextras\Orm\Entity\Entity;

/**
 * @property int $id             {primary}
 */
class Game extends Entity
{
	public function __construct(int $id)
	{
		parent::__construct();
		
		$this->id = $id;
	}
	
	public function toApi(): int
	{
		return $this->id;
	}
	
}
