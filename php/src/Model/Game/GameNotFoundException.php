<?php declare(strict_types = 1);

namespace Gamee\Model\Game;

use Gamee\Model\EntityNotFoundException;

final class GameNotFoundException extends EntityNotFoundException
{
}
