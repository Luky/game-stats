<?php declare(strict_types = 1);

namespace Gamee\Model;

/**
 * @property \Gamee\Model\Game\GameRepository $game
 * @property \Gamee\Model\Score\ScoreRepository $score
 * @property \Gamee\Model\User\UserRepository $user
 */
final class Model extends \Nextras\Orm\Model\Model
{
}
