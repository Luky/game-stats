<?php declare(strict_types = 1);

namespace Gamee\Model;

class EntityNotFoundException extends ModelException
{
}
