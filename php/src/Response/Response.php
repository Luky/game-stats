<?php declare(strict_types = 1);

namespace Gamee\Response;

class Response
{
	public const RPC_VERSION = '2.0';
	private $response = [];
	
	/**
	 * @param string|int $id
	 */
	public function __construct($id = null)
	{
		$this->response = [
			'id'      => $id,
			'jsonrpc' => self::RPC_VERSION,
		];
	}
	
	public function withSuccess($success): array
	{
		return $this->response + [
				'success' => $success,
			];
	}
	
	public function withError(int $code, string $message, $data = null): array
	{
		$this->response['error'] = [
			'code'    => $code,
			'message' => $message,
		];
		
		if ($data !== null) {
			$this->response['error']['data'] = $data;
		}
		
		return $this->response;
	}
	
	// -------------------------------------------------------------------------
	// Specific Error's
	// -------------------------------------------------------------------------
	
	public function withErrorParseError(): array
	{
		return $this->withError(-32700, 'Parse Error');
	}
	
	public function withErrorInvalidRequest(): array
	{
		return $this->withError(-32600, 'Invalid Request');
	}
	
	public function withErrorMethodNotFound(): array
	{
		return $this->withError(-32601, 'Method not found');
	}
	
	public function withErrorInvalidParams(): array
	{
		return $this->withError(-32602, 'Invalid params');
	}
}
