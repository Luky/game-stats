<?php declare(strict_types = 1);

namespace Gamee\Facade;

use Gamee\Model\User\User;
use Gamee\Model\User\UserNotFoundException;
use Nette\Utils\Random;
use Nette\Utils\Strings;

class UserFacade
{
	/** @var \Gamee\Model\User\UserRepository */
	private $userRepository;
	
	public function __construct(\Gamee\Model\User\UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}
	
	/**
	 * @throws \Gamee\Model\User\UserNotFoundException
	 * @throws \Nextras\Orm\InvalidArgumentException
	 */
	public function get(int $id): User
	{
		$game = $this->userRepository->getById($id);
		
		if ($game === null) {
			throw new UserNotFoundException();
		}
		
		return $game;
	}
	
	
	public function getOrCreate(int $id): User
	{
		$user = $this->userRepository->getById($id);
		
		if ($user === null) {
			$user = $this->create($id);
		}
		
		return $user;
	}
	
	public function create(int $id): User
	{
		$randomName = Random::generate(10);
		
		return new User($id, $randomName);
		
	}
	
	
}
