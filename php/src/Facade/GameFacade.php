<?php declare(strict_types = 1);

namespace Gamee\Facade;

use Gamee\Model\Game\Game;
use Gamee\Model\Game\GameNotFoundException;
use Gamee\Model\Game\GameRepository;

class GameFacade
{
	/** @var GameRepository */
	private $gameRepository;
	
	/**
	 */
	public function __construct(\Gamee\Model\Game\GameRepository $gameRepository)
	{
		$this->gameRepository = $gameRepository;
	}
	
	/**
	 * @throws \Gamee\Model\Game\GameNotFoundException
	 * @throws \Nextras\Orm\InvalidArgumentException
	 */
	public function get(int $id): Game
	{
		$game = $this->gameRepository->getById($id);
		
		if ($game === null) {
			throw new GameNotFoundException();
		}
		
		return $game;
	}
	
	
	public function getOrCreate(int $id): Game
	{
		$game = $this->gameRepository->getById($id);
		
		if ($game === null) {
			$game = $this->create($id);
			
			$this->gameRepository->persist($game);
		}
		
		return $game;
	}
	
	public function create(int $id): Game
	{
		return new Game($id);
	}
}
