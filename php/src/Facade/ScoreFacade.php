<?php declare(strict_types = 1);

namespace Gamee\Facade;

use Gamee\Model\Game\Game;
use Gamee\Model\Score\Score;
use Gamee\Model\User\User;
use Gamee\PositionCounter;
use Nextras\Orm\Collection\ICollection;

class ScoreFacade
{
	/** @var \Gamee\Facade\UserFacade */
	private $userFacade;
	/** @var \Gamee\Facade\GameFacade */
	private $gameFacade;
	/** @var \Gamee\Model\Score\ScoreRepository */
	private $scoreRepository;
	
	/**
	 */
	public function __construct(
		\Gamee\Facade\UserFacade $userFacade,
		\Gamee\Facade\GameFacade $gameFacade,
		\Gamee\Model\Score\ScoreRepository $scoreRepository
	)
	{
		$this->userFacade = $userFacade;
		$this->gameFacade = $gameFacade;
		$this->scoreRepository = $scoreRepository;
	}
	
	/**
	 * @throws \Gamee\Model\Game\GameNotFoundException
	 * @throws \Gamee\Model\User\UserNotFoundException
	 * @throws \Nextras\Orm\InvalidArgumentException
	 */
	public function process(int $gameId, int $userId, int $points): int
	{
		$game = $this->gameFacade->get($gameId);
		$user = $this->userFacade->get($userId);
		
		$score = $this->create($points, $user, $game);
		
		$this->scoreRepository->persistAndFlush($score);
		
		return 0;
	}
	
	/**
	 *
	 */
	public function create(int $score, User $user, Game $game): Score
	{
		return new Score($score, $user, $game);
	}
	
	/**
	 * @throws \Gamee\ValueDoesntExistsInPositionCounterException
	 */
	public function getTop(int $gameId, int $limit = 10): array
	{
		$res = $this->scoreRepository->findBy(
			[
				'game' => $gameId,
			]
		)
			->limitBy($limit)
			->orderBy('score', ICollection::DESC);

		$data = [];

		$counter = new PositionCounter($res->fetchPairs(null, 'score'));
		
		/** @var Score $row */
		foreach ($res as $i => $row) {
			$data[] = ['position' => $counter->getPosition($row->score)] + $row->toApi();
		}
		
		return $data;
	}
}
