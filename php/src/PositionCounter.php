<?php declare(strict_types = 1);

namespace Gamee;

class PositionCounter
{
	/** @var int[] */
	private $data;
	
	/**
	 * @param int[] $set
	 */
	public function __construct(array $set)
	{
		$this->data = $set;
		
		$prevScore = null;
		$prevPosition = 0;
		$offset = 1;
		
		\rsort($set);
		
		foreach ($set as $score) {
			
			// Same position for same score
			if ($prevScore > $score || $prevScore === null) {
				$prevScore = $score;
				$prevPosition = $offset;
				$this->data[$score] = $offset;
			} else {
				$this->data[] = $prevPosition;
			}
			
			$offset++;
		}
	}
	
	public function getPosition(int $score): int
	{
		if (isset($this->data[$score]) === false) {
			throw new ValueDoesntExistsInPositionCounterException(
				"Value '$score' doesn't exists in given PositionCounter set."
			);
		}
		
		return $this->data[$score];
	}
}
