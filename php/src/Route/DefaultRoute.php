<?php declare(strict_types = 1);

namespace Gamee\Route;

use Gamee\Model\Game\GameNotFoundException;
use Gamee\Model\User\UserNotFoundException;
use Luky\Slim\RouteInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class DefaultRoute implements RouteInterface
{
	/** @var \Gamee\Facade\ScoreFacade */
	private $scoreFacade;
	
	public function __construct(\Gamee\Facade\ScoreFacade $scoreFacade)
	{
		$this->scoreFacade = $scoreFacade;
	}
	
	public function create(\Slim\App $app): void
	{
		$app->post(
			'/',
			function (Request $request, Response $response): Response {
				/** @var array|null $body */
				$body = $request->getParsedBody();
				
				if ($body === null) {
					return $response->withJson(
						(new \Gamee\Response\Response())->withErrorParseError()
					);
				}
				
				if (\is_array($body) === false) {
					return $response->withJson(
						(new \Gamee\Response\Response())->withErrorInvalidRequest()
					);
				}
				
				if (isset($body['jsonrpc'])) {
					return $response->withJson($this->processEntry($body));
				}
				
				if (empty($body)) {
					return $response->withJson(
						(new \Gamee\Response\Response())->withErrorInvalidRequest()
					);
				}
				
				try {
					$batch = [];
					
					foreach ($body as $rowBody) {
						$batch[] = $this->processEntry($rowBody);
					}
					
					return $response->withJson($batch);
				} catch (\TypeError $e) {
					return $response->withJson(
						(new \Gamee\Response\Response())->withErrorParseError()
					);
				}
			}
		);
	}
	
	
	/**
	 * @throws \Gamee\ValueDoesntExistsInPositionCounterException
	 * @throws \Nextras\Orm\InvalidArgumentException
	 */
	public function processEntry(array $obj): array
	{
		$id = $obj['id'] ?? null;
		
		$rpcResponse = new \Gamee\Response\Response($id);
		
		$version = $obj['jsonrpc'] ?? null;
		$method = $obj['method'] ?? null;
		$params = $obj['params'] ?? null;
		
		if ($version !== \Gamee\Response\Response::RPC_VERSION) {
			return $rpcResponse->withError(30, 'Invalid Json RPC version');
		}
		
		if ($method === 'send_score') {
			$user = $params['user_id'] ?? null;
			$game = $params['game_id'] ?? null;
			$score = $params['score'] ?? null;
			
			
			if ($game === null || $user === null || $score === null) {
				return $rpcResponse->withErrorInvalidParams();
			}
			
			try {
				return $rpcResponse->withSuccess(
					$this->scoreFacade->process((int) $game, (int) $user, (int) $score)
				);
			} catch (\TypeError $e) {
				return $rpcResponse->withErrorInvalidParams();
			} catch (UserNotFoundException $e) {
				return $rpcResponse->withError(-32801, "User with given id '$user' doesn't exists.");
			} catch (GameNotFoundException $e) {
				return $rpcResponse->withError(-32802, "Game with given id '$game' doesn't exists.");
			}
		}
		
		if ($method === 'get_highscore') {
			$game = $params['game_id'] ?? null;
			
			if ($game === null) {
				return $rpcResponse->withErrorInvalidParams();
			}
			
			return $rpcResponse->withSuccess(
				$this->scoreFacade->getTop((int) $game, 10)
			);
		}
		
		return $rpcResponse->withErrorMethodNotFound();
	}
}
